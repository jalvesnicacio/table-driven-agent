package agents;

import env.Action;
import env.Perception;

public interface Agent {

	Perception perceive();
	
	Action execute(Perception p);
	
	String getName();
	
	
	//AgentProgram getAgentProgram();
}
