package agents;

import java.util.List;

import env.Action;
import env.Perception;

public interface AgentProgram {
	/*
	 * Todo Programa executa uma função que, dada uma sequencia de percepções, retorna uma ação
	 */
	Action execute(Perception percept);

}
