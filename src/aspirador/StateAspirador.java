package aspirador;

import env.Perception;
import env.State;

public class StateAspirador implements State {
	
	private Situation stateA;
	private Situation stateB;
	private Location aspLocation;
	
	public StateAspirador(Situation s1, Situation s2, Location aspLoc) {
		this.stateA = s1;
		this.stateB = s2;
		this.aspLocation = aspLoc;
	}
	
	public void print(){
		//System.out.println("\n\nMudanças no Estado:");
		System.out.printf("Estado atual: [ %s | %s ]%n", this.stateA, this.stateB);
	}

	@Override
	public Perception getPerception() {
		if(this.aspLocation == Location.A){
			return new Perception(this.aspLocation, this.stateA);
		} else
		{
			return new Perception(this.aspLocation, this.stateB);
		}
	}

	@Override
	public void setState(Situation a, Situation b, Location asp) {
		this.stateA = a;
		this.stateB = b;
		this.aspLocation = asp;
	}

	public Situation getStateA() {
		return stateA;
	}

	public void setStateA(Situation stateA) {
		this.stateA = stateA;
	}

	public Situation getStateB() {
		return stateB;
	}

	public void setStateB(Situation stateB) {
		this.stateB = stateB;
	}

	public Location getAspLocation() {
		return aspLocation;
	}

	public void setAspLocation(Location aspLocation) {
		this.aspLocation = aspLocation;
	}
	
	
	
	

}
