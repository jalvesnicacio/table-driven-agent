package aspirador;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import agents.AgentProgram;
import env.Action;
import env.Perception;
import env.PerceptionsSet;

public class TableDrivenProgram implements AgentProgram {
	
	private PerceptionsSet perceptions = new PerceptionsSet();
	private Map<PerceptionsSet, Action> tableOfActions = new HashMap<PerceptionsSet, Action>();
	
	
	@Override
	public Action execute(Perception p){
	  this.perceptions.add(p);
	  if(this.tableOfActions.containsKey(this.perceptions)){
			return this.tableOfActions.get(perceptions);
		}
		else return new Action("NoOp");
	}
	
	public TableDrivenProgram()
	{
		PerceptionsSet percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		this.tableOfActions.put(percepts, new Action("Direita"));
		
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A,Situation.SUJO));
		this.tableOfActions.put(percepts, new Action("Aspirar"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.B, Situation.LIMPO));
		this.tableOfActions.put(percepts, new Action("Esquerda"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.B, Situation.SUJO));
		this.tableOfActions.put(percepts, new Action("Aspirar"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		percepts.add(new Perception(Location.B, Situation.SUJO));
		this.tableOfActions.put(percepts, new Action("Aspirar"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		percepts.add(new Perception(Location.B, Situation.LIMPO));
		this.tableOfActions.put(percepts, new Action("NoOp"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.SUJO));
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		this.tableOfActions.put(percepts, new Action("Direita"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.SUJO));
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		percepts.add(new Perception(Location.B, Situation.LIMPO));
		this.tableOfActions.put(percepts, new Action("NoOp"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.SUJO));
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		percepts.add(new Perception(Location.B, Situation.SUJO));
		this.tableOfActions.put(percepts, new Action("Aspirar"));
		
		percepts = new PerceptionsSet();
		percepts.add(new Perception(Location.A, Situation.SUJO));
		percepts.add(new Perception(Location.A, Situation.LIMPO));
		percepts.add(new Perception(Location.B, Situation.SUJO));
		percepts.add(new Perception(Location.B, Situation.LIMPO));
		this.tableOfActions.put(percepts, new Action("NoOp"));
	}
	
	/*public boolean hasPerceptionsKey(List<Perception> p)
	{
		Iterator<List<Perception>> keySetIterator = this.tableOfActions.keySet().iterator();
		while(keySetIterator.hasNext()){
			List<Perception> key = keySetIterator.next();
			if(this.perceptions.equals(key)){
				return true;
			}
			System.out.println("loop...");
		}
		System.out.println("Noop...");
		return false;
	}*/
	
	public void iterateTable(){
		Iterator<PerceptionsSet> keySetIterator = this.tableOfActions.keySet().iterator();
		while(keySetIterator.hasNext()){
			PerceptionsSet key = keySetIterator.next();
			System.out.println("key: " + key + " value: " + this.tableOfActions.get(key));
		}
	}
	
}
	

	
