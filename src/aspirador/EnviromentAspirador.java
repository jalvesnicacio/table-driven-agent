package aspirador;

import java.util.ArrayList;
import java.util.List;

import agents.Agent;
import env.Action;
import env.Enviroment;
import env.Perception;
import env.State;

public class EnviromentAspirador implements Enviroment {
	
	private List<AgentAspirador> agents = new ArrayList<AgentAspirador>();
	
	//Instanciando o estado como Inicial: (situação A, Situação de B, Localização do agente)
	private StateAspirador estado = new StateAspirador(Situation.SUJO,Situation.SUJO,Location.A);
	private boolean done = false;
	

	@Override
	public Agent getAgent(Agent agent) {
		for (AgentAspirador asp : this.agents) {
			if(asp.getName() == agent.getName()){
				return asp;
			}
		}
		return null;
	}

	@Override
	public void addAgent(Agent agent) {
		this.agents.add((AgentAspirador) agent);
	}

	@Override
	public void removeAgent(Agent agent) {
		this.agents.remove((AgentAspirador) agent);
	}

	@Override
	public State getState() {
		return this.estado;
	}

	@Override
	public boolean isDone() {
		return this.done;
	}

	@Override
	public void step() {
		for (Agent ag : this.agents) {
			this.estado.print();
			
			Action act = ag.execute(estado.getPerception());
			
			switch (act.getName()) {
			case "Aspirar":
				if(this.estado.getAspLocation() == Location.A){
					this.estado.setStateA(Situation.LIMPO);
				}else{
					this.estado.setStateB(Situation.LIMPO);
				}
				break;
			
			case "Direita":
				this.estado.setAspLocation(Location.B);
				break;
				
			case "Esquerda":
				this.estado.setAspLocation(Location.A);
				break;
			case "NoOp": default:
				this.done = true;
				break;
			}	
			System.out.println("Ação: " + act.getName());
		}	
	}
	
	

}
