package aspirador;

import agents.Agent;
import agents.AgentProgram;
import env.Action;
import env.Perception;

public class AgentAspirador implements Agent{
	
	private String name;
	private Action aspirar = new Action("Aspirar");
	private Action direita = new Action ("Direira");
	private Action esquerda = new Action ("Esquerda");
	private Action noop = new Action("NoOp");
	private AgentProgram ap; 

	@Override
	public Action execute(Perception p) {
		Action act = this.ap.execute(p);
		return act;
	}

	@Override
	public Perception perceive() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public AgentAspirador(String name, AgentProgram ap) {
		this.name = name;
		this.ap = ap;
	}

	@Override
	public String getName() {
		return this.name;
	}
	
	
	
	
	

}
