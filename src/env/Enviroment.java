package env;

import java.util.ArrayList;
import java.util.List;

import agents.Agent;

public interface Enviroment {
	
	/*
	 * Retorna um agente que está no ambiente
	 */
	Agent getAgent(Agent agent);
	
	/*
	 * Inserir um agente no ambiente
	 */
	void addAgent(Agent agent);
	
	/*
	 * Remover agente:
	 */
	void removeAgent(Agent agent);
	
	/*
	 * retorna o estado atual do ambiente
	 */
	State getState();
	
	boolean isDone();
	
	void step();
	

}
