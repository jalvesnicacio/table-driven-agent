package env;

import java.util.ArrayList;
import java.util.List;

public class PerceptionsSet {
	private List<Perception> perceptions = new ArrayList<Perception>();

	public void add(Perception p) {
		this.perceptions.add(p);
		System.out.println("*======");
		System.out.println(this.toString());
		System.out.println("======*");
	}

	public void clear() {
		this.perceptions.clear();		
	}
	
	public List<Perception> getPerceptions()
	{
		return this.perceptions;
	}

	@Override
	public boolean equals(Object obj) {
		PerceptionsSet p = (PerceptionsSet) obj;
		if(this.getPerceptions().size() != p.getPerceptions().size()) {
			return false;
		}
		for(int i = 0; i < this.perceptions.size(); i++) {
			for(int j = 0; j < p.getPerceptions().size(); j++){
				if(! this.perceptions.get(i).equals(p.getPerceptions().get(j))){
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		String value = "";
		for (Perception percept : this.perceptions) {
			value += percept.hashCode();
		}
		return value.hashCode();
	}

	@Override
	public String toString() {
		String value = "";
		for (Perception percept : this.perceptions) {
			value += percept.toString() + " ";
		}
		return value + "\n";
	}
	
	

}
