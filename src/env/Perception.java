package env;

import aspirador.Location;
import aspirador.Situation;

public class Perception {
	
	private Location location;
	private Situation state;
	
	public Location getLocation() {
		return location;
	}
	
	public Situation getState() {
		return state;
	}

	public Perception(Location location, Situation state) {
		this.location = location;
		this.state = state;
	}

	@Override
	public boolean equals(Object obj) {
		Perception o = (Perception) obj;
		if(this.location.equals(o.getLocation()) && this.state.equals(o.getState()))
		{
			return true;
		}
			return false;
	}

	@Override
	public int hashCode() {
		String perceptString = this.location.name() + this.state.name();
		return perceptString.hashCode();
	}

	@Override
	public String toString() {
		return "[" + this.getLocation().name() + "|" + this.getState().name() + "]";
	}
	
	
	
	

}
