package env;

import aspirador.Location;
import aspirador.Situation;

public interface State {
	
	Perception getPerception();
	
	void setState(Situation a, Situation b, Location asp);
	

}
