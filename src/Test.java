import agents.Agent;
import aspirador.AgentAspirador;
import aspirador.EnviromentAspirador;
import aspirador.TableDrivenProgram;
import env.Enviroment;


public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Cria um ambiente, um agente e coloca o agente dentro do ambiente
		Enviroment env  = new EnviromentAspirador();
		Agent a = new AgentAspirador("R2D2", new TableDrivenProgram());
		env.addAgent(a);
		
		while (!env.isDone())
		{
			env.step();
		}

	}

}
